#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2014-09-22 00:24+0100\n"
"Last-Translator: Willy Raets <willy@develop.earthshipeurope.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "DNS client example"
msgstr "DNS cliënt voorbeeld"

#: FMain.class:14
msgid "Not Found"
msgstr "Niet gevonden"

#: FMain.form:29
msgid "DnsClient Example"
msgstr "DnsCliënt Voorbeeld"

#: FMain.form:34
msgid "Go!"
msgstr "-"

#: FMain.form:44
msgid "gambas.sourceforge.net"
msgstr "-"

#: FMain.form:49
msgid "www.kudla.org"
msgstr "-"

#: FMain.form:54
msgid "www.freshmeat.net"
msgstr "-"

#: FMain.form:95
msgid "Cancel"
msgstr "Annuleer"

#: FMain.form:100
msgid "www.slashdot.org"
msgstr "-"

#: FMain.form:105
msgid "www.rpmfind.net"
msgstr "-"

#: FMain.form:110
msgid "www.debian.org"
msgstr "-"

#: FMain.form:115
msgid "www.mandrake.com"
msgstr "-"

#: FMain.form:154
msgid "Host Name to IP"
msgstr "Host naam naar IP"

#: FMain.form:154
msgid "IP to Host Name"
msgstr "IP naar Host naam"

#: FMain.form:159
msgid "<< Copy"
msgstr "<< Kopiëer"

#: FMain.form:164
msgid "Asynchronous"
msgstr "Asynchrone"

#: FMain.class:217
msgid "Finished"
msgstr "Beëindigt"

#: FMain.class:251
msgid "Cancelled"
msgstr "Geannuleerd"

#: FMain.class:279
msgid "Write here 7 host names then press GO!"
msgstr "Noteer hier 7 host namen en druk GO!"

#: FMain.class:281
msgid "Write here 7 IP addresses then press GO!"
msgstr "Noteer hier 7 IP adressen en druk GO!"

