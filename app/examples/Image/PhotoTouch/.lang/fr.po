#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Simple Photo Editor"
msgstr "Editeur de photographie simplifié"

#: .project:2
msgid "This is a simple photo editor I made for my father. It is too complex for him, but it is still a good Gambas example!"
msgstr ""

#: FBrightness.form:40 FMain.class:200
msgid "Balance"
msgstr "Balance"

#: FMain.class:107
msgid "No image in directory"
msgstr "Aucune image dans ce répertoire"

#: FMain.class:109
msgid "Unable to load image"
msgstr "Impossible de charger l'image"

#: FMain.class:200
msgid "Automatic correction"
msgstr "Correction automatique"

#: FMain.class:200
msgid "Blur"
msgstr "Flou"

#: FMain.class:200
msgid "Browse photos"
msgstr "Parcourir les photos"

#: FMain.class:200
msgid "Crop image"
msgstr "Découper l'image"

#: FMain.class:200
msgid "Fit to window"
msgstr "Zoom à la taille de l'écran"

#: FMain.class:200
msgid "Flip horizontally"
msgstr "Retourner horizontalement"

#: FMain.class:200
msgid "Flip vertically"
msgstr "Retourner verticalement"

#: FMain.class:200
msgid "Invert"
msgstr "Inverser"

#: FMain.class:200
msgid "Normalize"
msgstr "Normaliser"

#: FMain.class:200
msgid "Oil painting effect"
msgstr "Effet de peinture à l'huile"

#: FMain.class:200
msgid "Quit"
msgstr "Quitter"

#: FMain.class:200
msgid "Remove speckles"
msgstr "Enlever les tâches"

#: FMain.class:200 FResize.form:42
msgid "Resize"
msgstr "Redimensionner"

#: FMain.class:200
msgid "Rotate left"
msgstr "Rotation vers la gauche"

#: FMain.class:200
msgid "Rotate right"
msgstr "Rotation vers la droite"

#: FMain.class:200
msgid "Save"
msgstr "Enregistrer"

#: FMain.class:200
msgid "Save all"
msgstr "Tout enregistrer"

#: FMain.class:200
msgid "Select photo directory"
msgstr "Choisir le répertoire des photographies"

#: FMain.class:200
msgid "Sharpen"
msgstr "Netteté"

#: FMain.class:200
msgid "Show photo"
msgstr "Afficher la photographie"

#: FMain.class:200
msgid "Undo all changes"
msgstr "Annuler tous les changements"

#: FMain.class:200
msgid "Zoom 100%"
msgstr "Zoom 100%"

#: FMain.class:200
msgid "Zoom in"
msgstr "Zoom avant"

#: FMain.class:200
msgid "Zoom out"
msgstr "Zoom arrière"

#: FMain.class:222
msgid "*"
msgstr "*"

#: FMain.class:222
msgid "/"
msgstr "/"

#: FMain.class:222
msgid "B"
msgstr "F"

#: FMain.class:222
msgid "D"
msgstr "T"

#: FMain.class:222
msgid "E"
msgstr "E"

#: FMain.class:222
msgid "H"
msgstr "H"

#: FMain.class:222
msgid "I"
msgstr "I"

#: FMain.class:222
msgid "M"
msgstr "M"

#: FMain.class:222
msgid "N"
msgstr "N"

#: FMain.class:222
msgid "O"
msgstr "P"

#: FMain.class:222
msgid "R"
msgstr "R"

#: FMain.class:222
msgid "S"
msgstr "S"

#: FMain.class:222
msgid "V"
msgstr "V"

#: FScissors.form:24
msgid "Cut"
msgstr "Découper"
