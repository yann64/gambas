#Willy Raets < gbWilly@openmailbox.org >, 2015
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.report2 3.10.90\n"
"PO-Revision-Date: 2017-08-26 19:45 UTC\n"
"Last-Translator: Willy Raets <gbWilly@openmailbox.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project: 1
msgid "ReportsEvolution"
msgstr "Rapport Evolutie"

#: FPreview.class: 212
msgid "PDF files"
msgstr "PDF bestanden"

#: FPreview.class: 212
msgid "Postscript files"
msgstr "Postscript bestanden"

#: FPreview.class: 227
msgid "Cancel"
msgstr "Annuleer"

#: FPreview.class: 317
msgid "Replace"
msgstr "Vervangen"

#: FPreview.class: 317
msgid "This file already exists.\nDo you want to replace it?"
msgstr "Dit bestand bestaat reeds.\nWil je het vervangen?"

#: FPreview.class: 342
msgid "Layout..."
msgstr "-"

#: FPreview.form:79
msgid "Report preview"
msgstr "Rapport voorbeeld"

#: FPreview.form:86
msgid "Print to file"
msgstr "Afdrukken naar bestand"

#: FPreview.form:115
msgid "Printing..."
msgstr "Afdrukken..."

#: FPreview.form:145
msgid "One page"
msgstr "Een pagina"

#: FPreview.form:154
msgid "Two pages"
msgstr "Twee pagina's"

#: FPreview.form:163
msgid "Full width"
msgstr "Volledige breedte"

#: FPreview.form:172
msgid "Real size"
msgstr "Echte afmeting"

#: FPreview.form:180
msgid "Zoom out"
msgstr "Zoom uit"

#: FPreview.form:193
msgid "100 %"
msgstr "-"

#: FPreview.form:199
msgid "Zoom in"
msgstr "-"

#: FPreview.form:208
msgid "Print"
msgstr "Afdrukken"

#: FPreview.form:216
msgid "Show options"
msgstr "Opties weergeven"

#: FPreview.form:239
msgid "Printer"
msgstr "Printer"

#: FPreview.form:264
msgid "Two-sided"
msgstr "Dubbelzijdig"

#: FPreview.form:271
msgid "Long side"
msgstr "Lange zijde"

#: FPreview.form:271
msgid "None"
msgstr "Geen"

#: FPreview.form:271
msgid "Short side"
msgstr "Korte zijde"

#: FPreview.form:275
msgid "File"
msgstr "Bestand"

#: FPreview.form:301
msgid "Resolution"
msgstr "Resolutie"

#: FPreview.form:308
msgid "1200"
msgstr "-"

#: FPreview.form:308
msgid "150"
msgstr "-"

#: FPreview.form:308
msgid "300"
msgstr "-"

#: FPreview.form:308
msgid "600"
msgstr "-"

#: FPreview.form:308
msgid "75"
msgstr "-"

#: FPreview.form:313
msgid "dpi"
msgstr "-"

#: FPreview.form:336
msgid "Range"
msgstr "Bereik"

#: FPreview.form:352
msgid "Copies"
msgstr "Kopieën"

#: FPreview.form:372
msgid "Orientation"
msgstr "Oriëntatie"

#: FPreview.form:379
msgid "Landscape"
msgstr "Landschap"

#: FPreview.form:379
msgid "Portrait"
msgstr "Portret"

#: FPreview.form:389
msgid "Paper"
msgstr "Papier"

#: FPreview.form:396
msgid "A3"
msgstr "-"

#: FPreview.form:396
msgid "A4"
msgstr "-"

#: FPreview.form:396
msgid "A5"
msgstr "-"

#: FPreview.form:396
msgid "B5"
msgstr "-"

#: FPreview.form:396
msgid "Custom"
msgstr "Aangepast"

#: FPreview.form:396
msgid "Executive"
msgstr "-"

#: FPreview.form:396
msgid "Legal"
msgstr "-"

#: FPreview.form:396
msgid "Letter"
msgstr "-"

#: FPreview.form:411
msgid "Width"
msgstr "Breedte"

#: FPreview.form:422
msgid "mm"
msgstr "-"

#: FPreview.form:432
msgid "Height"
msgstr "Hoogte"

#: FPreview.form:455
msgid "Grayscale"
msgstr "Grijswaarden"

#: FPreview.form:461
msgid "Full page"
msgstr "Volledige pagina"

#: FPreview.form:466
msgid "Collate copies"
msgstr "Sorteer kopieën"

#: FPreview.form:471
msgid "Reverse order"
msgstr "Omgekeerde volgorde"

#: OutputReport2.report:77
msgid "Version 1.0"
msgstr "-"

#: OutputReport2.report:84 Report51.report:60
msgid "Date"
msgstr "Datum"

#: OutputReport2.report:99
msgid "Project Title:"
msgstr "-"

#: OutputReport2.report:106
msgid "Project No.:"
msgstr "-"

#: OutputReport2.report:113
msgid "Company:"
msgstr "-"

#: OutputReport2.report:120
msgid "Designer:"
msgstr "Ontwerper:"

#: OutputReport2.report:134
msgid "Base Plate ID:"
msgstr "-"

#: OutputReport2.report:185
msgid "Bearing Pressue"
msgstr "-"

#: OutputReport2.report:195
msgid "Node #"
msgstr "Knoop #"

#: OutputReport2.report:202
msgid "Brg. Press., psi"
msgstr "-"

#: OutputReport2.report:266
msgid "Anchor Rod Tension"
msgstr "-"

#: OutputReport2.report:276
msgid "Rod #"
msgstr "-"

#: OutputReport2.report:283
msgid "Tension, lbs"
msgstr "-"

#: OutputReport2.report:353
msgid "Page $PAGE of $NPAGE"
msgstr "Pagina $PAGE van $NPAGE"

#: Report.class:112
msgid "Section "
msgstr "Sectie"

#: Report1.report:29
msgid "=\"Page \" & Page"
msgstr "-"

#: Report1.report:46
msgid "=pi() + 4"
msgstr "-"

#: Report10.report:22 Report14.report:53
msgid "=index"
msgstr "-"

#: Report10.report:31
msgid "=Index"
msgstr "-"

#: Report10.report:46
msgid "=\"Index = \" & index"
msgstr "-"

#: Report10.report:54
msgid "=page & \" / \" & pages"
msgstr "-"

#: Report11.report:19 Report12.report:17
msgid "Section 1"
msgstr "-"

#: Report11.report:39
msgid "Gambas"
msgstr "-"

#: Report11.report:59
msgid "All friends list !"
msgstr "Alle vrienden lijst!"

#: Report11.report:81
msgid "Gambas Report Demo"
msgstr "Gambas Rapport Demo"

#: Report11.report:93
msgid "DEMO"
msgstr "-"

#: Report11.report:106
msgid "Page $PAGE on $NPAGE  "
msgstr "Pagina $PAGE van $NPAGE  "

#: Report13.report:36
msgid "=\"GAMBAS - \" & index"
msgstr "-"

#: Report13.report:44
msgid "=\"PAGE \" & Page & \" / \" & pages"
msgstr "-"

#: Report14.report:28
msgid "Reference"
msgstr "Referentie"

#: Report14.report:35
msgid "Description"
msgstr "Omschrijving"

#: Report14.report:42
msgid "Valeur"
msgstr "-"

#: Report14.report:73
msgid "=page & \" on \" & pages"
msgstr "-"

#: Report16.report:21 Report17.report:22 Report5.report:20
msgid "Coucou"
msgstr "-"

#: Report2.report:19
msgid "ReportLabel1"
msgstr "-"

#: Report3.report:18
msgid "PARCELLAIRE $NPAGE"
msgstr "-"

#: Report51.report:30
msgid "Recto"
msgstr "-"

#: Report51.report:38
msgid "Verso"
msgstr "-"

#: Report51.report:51
msgid "N°"
msgstr "-"

#: Report51.report:99
msgid "Observations"
msgstr "Observaties"

#: Report6.report:12
msgid "<Font bold=1><b>Test</b></font>test<br><br>\n\n<table border=1 cellspacing=0><tr><td><td width=20></td></td></tr></table>"
msgstr "-"

